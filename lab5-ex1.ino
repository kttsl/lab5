#include "ESP8266WiFi.h"

int n = 0;
void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  
  n = WiFi.scanNetworks();
  if (n == 0) {
    Serial.println("no networks found");
  }
  else {
    for (int i = 0; i < n; ++i) {
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
}

String str = "";
int step_number = 0;
char ssid[30] = "";
char pass[30] = "";
void loop() {
  if (Serial.available() > 0) {
    char c = Serial.read();
  
    if (c != '\0' && c != '\n'){
      str += c;
    }
    else{
      if (step_number == 0){
        int tmp = str.toInt();
        if (tmp > n) Serial.println("Nhap sai");
        else {
          WiFi.SSID(tmp-1).toCharArray(ssid, 30);
          step_number += 1;
          Serial.print("Nhap password cho ");
          Serial.println(ssid);
          str = "";
        }
      }
      else if (step_number == 1){
        step_number = 2;
        str.toCharArray(pass, 30);
        WiFi.begin(ssid, pass);
        while (WiFi.status() != WL_CONNECTED){
              delay(500);
              Serial.print(".");
        }
        Serial.println("");
        Serial.println("WiFi connected");
      }
    }
  }
}
